package com.exadel.recommendation.service;

import com.exadel.recommendation.model.HighestCrypto;
import com.exadel.recommendation.model.Statistics;

import java.time.LocalDate;
import java.util.List;

public interface CryptoService {
    List<String> getDescendingOrderCrypto();

    Statistics getStatistics(String cryptoSymbol);

    HighestCrypto getHighestNormalizedRange(LocalDate date);
}
