package com.exadel.recommendation.controller;

import com.exadel.recommendation.model.HighestCrypto;
import com.exadel.recommendation.model.Statistics;
import com.exadel.recommendation.service.CryptoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/v1/crypto")
@Api(tags = "Crypto Investment recommendation service APIs")
public class CryptoController {
    private final CryptoService cryptoService;

    @Autowired
    public CryptoController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @GetMapping("/sorted-list")
    @ApiOperation(value = "Returns a descending sorted list of all the cryptos, comparing the normalized range.",
            notes = "Returns a list of all crypto symbols.")
    public List<String> getDescendingOrder() {
        return cryptoService.getDescendingOrderCrypto();
    }

    @GetMapping("/statistics/{cryptoSymbol}")
    @ApiOperation(value = "Returns the oldest/newest/min/max values for a requested crypto.",
            notes = "Returns a statistics for crypto.")
    public Statistics getStatistics(
            @PathVariable @ApiParam(value = "The crypto name for which to retrieve the crypto statistics. Example cryptoSymbol is 'BTC'.") String cryptoSymbol) {
        return cryptoService.getStatistics(cryptoSymbol);
    }

    @GetMapping("/highest-range/{date}")
    @ApiOperation(value = "Returns the crypto with the highest normalized range for a specific day.",
            notes = "Returns a statistics for crypto.")
    public HighestCrypto getCryptoWithHighestNormalizedRange(
            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @ApiParam(value = "The specific date for which to retrieve the crypto data. Example date is '2024-01-01'.") LocalDate date) {
        return cryptoService.getHighestNormalizedRange(date);
    }

}