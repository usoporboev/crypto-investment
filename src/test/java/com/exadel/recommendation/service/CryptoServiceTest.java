//package com.exadel.recommendation.service;
//
//import com.exadel.recommendation.Application;
//import com.exadel.recommendation.model.CryptoData;
//import com.exadel.recommendation.model.HighestCrypto;
//import com.exadel.recommendation.model.Statistics;
//import org.junit.BeforeClass;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.time.LocalDate;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//class CryptoServiceTest {
//
//    @Mock
//    private CryptoServiceImpl  cryptoService;
//
//    @Mock
//    private ResourceService resourceService;
//
//    @BeforeEach
//    void setUp() {
//        MockitoAnnotations.initMocks(this);
//        when(resourceService.readAllFiles()).thenReturn(getMockCryptoMap());
//    }
//
//    @Test
//    void testGetDescendingOrderCrypto() {
//        List<String> descendingOrderCrypto = cryptoService.getDescendingOrderCrypto();
//        assertEquals(Arrays.asList("ETH", "BTC", "LTC"), descendingOrderCrypto);
//    }
//
//    @Test
//    void testGetStatistics() {
//        Statistics statistics = cryptoService.getStatistics("BTC");
//
//        assertEquals(3.0, statistics.getMax());
//        assertEquals(1.0, statistics.getMin());
//        assertEquals(1.0, statistics.getOldest());
//        assertEquals(3.0, statistics.getNewest());
//    }
//
//    @Test
//    void testGetHighestNormalizedRange() {
//        LocalDate date = LocalDate.now().minusDays(1);
//        HighestCrypto highestCrypto = cryptoService.getHighestNormalizedRange(date);
//
//        assertEquals("ETH", highestCrypto.getCrypto());
//        assertEquals(1.0, highestCrypto.getNormalizedRange());
//    }
//
//    private Map<String, List<CryptoData>> getMockCryptoMap() {
//        CryptoData btcData1 = new CryptoData().setTimestamp(1L).setSymbol("BTC").setPrice(1.0);
//        CryptoData btcData2 = new CryptoData().setTimestamp(2L).setSymbol("BTC").setPrice(2.0);
//        CryptoData btcData3 = new CryptoData().setTimestamp(3L).setSymbol("BTC").setPrice(3.0);
//        CryptoData ethData1 = new CryptoData().setTimestamp(1L).setSymbol("ETH").setPrice(1.0);
//        CryptoData ethData2 = new CryptoData().setTimestamp(2L).setSymbol("ETH").setPrice(2.0);
//        CryptoData ethData3 = new CryptoData().setTimestamp(3L).setSymbol("ETH").setPrice(3.0);
//        CryptoData ltcData1 = new CryptoData().setTimestamp(1L).setSymbol("LTC").setPrice(1.0);
//        CryptoData ltcData2 = new CryptoData().setTimestamp(2L).setSymbol("LTC").setPrice(2.0);
//        CryptoData ltcData3 = new CryptoData().setTimestamp(3L).setSymbol("LTC").setPrice(3.0);
//
//        return new HashMap<>() {{
//            put("BTC", Arrays.asList(btcData1, btcData2, btcData3));
//            put("ETH", Arrays.asList(ethData1, ethData2, ethData3));
//            put("LTC", Arrays.asList(ltcData1, ltcData2, ltcData3));
//        }};
//    }
//}