package com.exadel.recommendation.service;

import com.exadel.recommendation.model.CryptoData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResourceService {

    @Value("${price-folder-path}")
    private String priceFolderPath;

    public Map<String, List<CryptoData>> readAllFiles() {
        Map<String, List<CryptoData>> cryptoMap = new HashMap<>();
        try {
            File[] listFiles = ResourceUtils.getFile(priceFolderPath).getAbsoluteFile().listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    String name = file.getName();
                    String symbol = name.split("_")[0];
                    cryptoMap.put(symbol, readCsvFile(file));

                    System.out.println(name);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cryptoMap;
    }

    public List<CryptoData> readCsvFile(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            List<CryptoData> cryptoDataList = new ArrayList<>();
            reader.lines()
                    .skip(1)
                    .filter(line -> line.split(",").length == 3)
                    .forEach(line -> {
                        String[] data = line.split(",");
                        cryptoDataList.add(new CryptoData()
                                .setTimestamp(Long.parseLong(data[0].trim()))
                                .setSymbol(data[1].trim())
                                .setPrice(Double.parseDouble(data[2].trim())));
                        //todo: remove
//                        CryptoUtil.printDate(new Crypto()
//                                .setTimestamp(Long.parseLong(data[0].trim()))
//                                .setSymbol(data[1].trim())
//                                .setPrice(Double.parseDouble(data[2].trim())));
                    });
            return cryptoDataList;
        }
    }
}
