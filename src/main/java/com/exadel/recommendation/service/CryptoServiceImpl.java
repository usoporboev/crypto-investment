package com.exadel.recommendation.service;

import com.exadel.recommendation.model.CryptoData;
import com.exadel.recommendation.model.Statistics;
import com.exadel.recommendation.model.HighestCrypto;
import com.exadel.recommendation.util.CryptoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Service
public class CryptoServiceImpl implements CryptoService {

    private Map<String, List<CryptoData>> cryptoMap;

    @Autowired
    public void setResourceService(ResourceService resourceService) {
        cryptoMap = resourceService.readAllFiles();
    }

    @Override
    public List<String> getDescendingOrderCrypto() {
        Map<String, Double> avgCrypto = new HashMap<>();
        cryptoMap.forEach((k, v) -> avgCrypto.put(k, CryptoUtil.getNormalizedRange(v)));

        return avgCrypto.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(Map.Entry::getKey).collect(Collectors.toList());
    }

    @Override
    public Statistics getStatistics(String cryptoSymbol) {
        return new Statistics()
                .setMax(CryptoUtil.getMax(CryptoUtil.getOrEmpty(cryptoMap, cryptoSymbol)))
                .setMin(CryptoUtil.getMin(CryptoUtil.getOrEmpty(cryptoMap, cryptoSymbol)))
                .setNewest(CryptoUtil.getNewest(CryptoUtil.getOrEmpty(cryptoMap, cryptoSymbol)))
                .setOldest(CryptoUtil.getOldest(CryptoUtil.getOrEmpty(cryptoMap, cryptoSymbol)));
    }

    @Override
    public HighestCrypto getHighestNormalizedRange(LocalDate date) {
        return cryptoMap.entrySet()
                .stream()
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> CryptoUtil.getNormalizedRangeByDate(entry.getValue(), date)))
                .entrySet().stream().max(Map.Entry.comparingByValue())
                .map(entry -> new HighestCrypto().setCrypto(entry.getKey()).setNormalizedRange(entry.getValue()))
                .orElse(null);
    }
}