package com.exadel.recommendation.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class HighestCrypto {
    private String crypto;
    double normalizedRange;
}
