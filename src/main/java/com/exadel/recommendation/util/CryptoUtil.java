package com.exadel.recommendation.util;

import com.exadel.recommendation.model.CryptoData;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CryptoUtil {

    public static final double DEFAULT_ZERO_PRICE = 0.0;

    public static double getMax(List<CryptoData> cryptoData) {
        return cryptoData.stream()
                .max(Comparator.comparingDouble(CryptoData::getPrice))
                .map(CryptoData::getPrice)
                .orElse(DEFAULT_ZERO_PRICE);
    }

    public static double getMin(List<CryptoData> cryptoData) {
        return cryptoData.stream()
                .min(Comparator.comparingDouble(CryptoData::getPrice))
                .map(CryptoData::getPrice)
                .orElse(DEFAULT_ZERO_PRICE);
    }

    public static double getOldest(List<CryptoData> cryptoData) {
        return cryptoData.stream()
                .min(Comparator.comparingLong(CryptoData::getTimestamp))
                .map(CryptoData::getPrice)
                .orElse(DEFAULT_ZERO_PRICE);
    }

    public static double getNewest(List<CryptoData> cryptoData) {
        return cryptoData.stream()
                .max(Comparator.comparingLong(CryptoData::getTimestamp))
                .map(CryptoData::getPrice)
                .orElse(DEFAULT_ZERO_PRICE);
    }

    public static double getNormalizedRange(List<CryptoData> cryptoData) {
        double maxPrice = getMax(cryptoData);
        double minPrice = getMin(cryptoData);

        if (minPrice > DEFAULT_ZERO_PRICE) {
            return (maxPrice - minPrice) / minPrice;
        }
        return DEFAULT_ZERO_PRICE;
    }

    public static double getNormalizedRangeByDate(List<CryptoData> cryptoData, LocalDate date) {
        List<CryptoData> filteredCryptoData = cryptoData.stream()
                .filter(crypto -> isDateEquals(date, crypto.getTimestamp()))
                .collect(Collectors.toList());
        return getNormalizedRange(filteredCryptoData);
    }

    public static boolean isDateEquals(LocalDate date, long timestamp) {
        return Instant.ofEpochMilli(timestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate().equals(date);
    }

    public static void printDate(CryptoData cryptoData) {
        Instant instant = Instant.ofEpochMilli(cryptoData.getTimestamp());
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        String formattedDate = dateTime.format(formatter);
        System.out.println(cryptoData.getSymbol() + ":" + formattedDate + ":  " + cryptoData.getPrice());
    }

    public static List<CryptoData> getOrEmpty(Map<String, List<CryptoData>> cryptoMap, String key) {
        return cryptoMap.getOrDefault(key, List.of());
    }
}
