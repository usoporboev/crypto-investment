package com.exadel.recommendation.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Statistics {
    double oldest;
    double newest;
    double min;
    double max;
}
