FROM maven:3.8-openjdk-17 AS build
LABEL authors="usoporboyev"

COPY . /home/app/src
WORKDIR /home/app/src
RUN mvn clean package -B -Dmaven.test.skip=true

FROM openjdk:17-jdk-alpine
RUN mkdir /app
COPY --from=build /home/app/src/target/*.jar /app/recommendation-service.jar
ENTRYPOINT ["java", "-jar", "/app/recommendation-service.jar"]
