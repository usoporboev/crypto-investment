//package com.exadel.recommendation.filter;
//
//import io.github.bucket4j.Bucket;
//import io.github.bucket4j.Buckets;
//import io.github.bucket4j.Refill;
//import io.github.bucket4j.grid.GridBucketState;
//import io.github.bucket4j.grid.hazelcast.HazelcastBucket4jRegion;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.time.Duration;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Component
//public class RateLimitingFilter extends OncePerRequestFilter {
//
//    private final Map<String, Bucket> buckets = new ConcurrentHashMap<>();
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        String clientIp = getClientIP(request);
//        Bucket bucket = getBucket(clientIp);
//        if (bucket.tryConsume(1)) {
//            filterChain.doFilter(request, response);
//        } else {
//            response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
//        }
//    }
//
//    private String getClientIP(HttpServletRequest request) {
//        // Implement logic to extract client IP from the request
//        return request.getRemoteAddr();
//    }
//
//    private Bucket getBucket(String clientIp) {
//        return buckets.computeIfAbsent(clientIp, key -> createNewBucket());
//    }
//
//    private Bucket createNewBucket() {
//        Refill refill = Refill.greedy(10, Duration.ofMinutes(1));
//        return Buckets.withLimitedBandwidth(refill).build();
//    }
//}