package com.exadel.recommendation.util;

import com.exadel.recommendation.model.CryptoData;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CryptoUtilTest {

    @Test
    void testGetMax() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, 1_000_000L),
                new CryptoData("ETH", 2.0, 1_100_000L),
                new CryptoData("LTC", 3.0, 1_200_000L)
        );

        double maxPrice = CryptoUtil.getMax(cryptoDataList);
        assertEquals(3.0, maxPrice);
    }

    @Test
    void testGetMin() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, 1_000_000L),
                new CryptoData("ETH", 2.0, 1_100_000L),
                new CryptoData("LTC", 0.5, 1_200_000L)
        );

        double minPrice = CryptoUtil.getMin(cryptoDataList);
        assertEquals(0.5, minPrice);
    }

    @Test
    void testGetOldest() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, 1_000_000L),
                new CryptoData("ETH", 2.0, 1_100_000L),
                new CryptoData("LTC", 3.0, 1_200_000L)
        );

        double oldestPrice = CryptoUtil.getOldest(cryptoDataList);
        assertEquals(1.0, oldestPrice);
    }

    @Test
    void testGetNewest() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, 1_000_000L),
                new CryptoData("ETH", 2.0, 1_100_000L),
                new CryptoData("LTC", 3.0, 1_200_000L)
        );

        double newestPrice = CryptoUtil.getNewest(cryptoDataList);
        assertEquals(3.0, newestPrice);
    }

    @Test
    void testGetNormalizedRange() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, 1_000_000L),
                new CryptoData("ETH", 2.0, 1_100_000L),
                new CryptoData("LTC", 3.0, 1_200_000L)
        );

        double normalizedRange = CryptoUtil.getNormalizedRange(cryptoDataList);
        assertEquals(2.0, normalizedRange);
    }

    @Test
    void testGetNormalizedRangeByDate() {
        List<CryptoData> cryptoDataList = Arrays.asList(
                new CryptoData("BTC", 1.0, LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()),
                new CryptoData("BTC", 2.0, LocalDate.now().minusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()),
                new CryptoData("BTC", 3.0, LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli())
        );

        double normalizedRange = CryptoUtil.getNormalizedRangeByDate(cryptoDataList, LocalDate.now().minusDays(1));
        assertEquals(1.0, normalizedRange);
    }

    @Test
    void testIsDateEquals() {
        LocalDate date = LocalDate.of(2022, 1, 1);
        long timestamp = date.atStartOfDay().toInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toEpochMilli();
        assertTrue(CryptoUtil.isDateEquals(date, timestamp));
    }
}